<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <title><?php
        global $page, $paged;
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";
        if ($paged >= 2 || $page >= 2)
            echo ' | ' . sprintf(__('Page %s', 'tie'), max($paged, $page));
        ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <!-- GoogleWebFonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Armata&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pirata+One&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gentium+Basic:400,700,400italic,700italic&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,700,300&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,500italic,700,700italic,900,900italic&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,600,600italic,700,700italic,900'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Advent+Pro:400,700,600,500,300&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gentium+Basic:400,700,400italic,700italic&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Dosis&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Belleza&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cuprum:400,700,700italic,400italic&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Jockey+One&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Magra:400,700&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Play:400,700&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Trocchi&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!--// GoogleWebFonts -->

    <?php wp_head(); ?>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold' rel='stylesheet' type='text/css'/>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery-ui.min.js'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery.maskedinput.min.js'></script>
	
	<script type="text/javascript">

/*** 
    Simple jQuery Slideshow Script
    Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)
***/

function slideSwitch() {
    var active = jQuery('#slideshow IMG.active');

    if ( active.length == 0 ) active = jQuery('#slideshow IMG:last');

    // use this to pull the images in the order they appear in the markup
    var next =  active.next().length ? active.next()
        : jQuery('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    active.addClass('last-active');

   next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            active.removeClass('active last-active');
        });
}


jQuery(document).ready(function(){
    setInterval( "slideSwitch()", 5000 );
	jQuery(".css_form_urlfield").mask("0 (999) 999 99 99");
});

</script>

<style type="text/css">

/*** set the width and height to match your images **/

#slideshow {
    position:relative;
    height:350px;
}

#slideshow IMG {
    position:absolute;
    top:0;
    left:0;
    z-index:8;
    opacity:0.0;
}

#slideshow IMG.active {
    z-index:10;
    opacity:1.0;
}

#slideshow IMG.last-active {
    z-index:9;
}

</style>
</head>
<?php global $is_IE ?>
<body id="top" <?php body_class(); ?>>
<?php if(function_exists(updateHeader)) updateHeader(); ?>
<div class="background-cover"></div>
<div class="wrapper">
    <header>
        <div style="float: left;
margin-left: 5px;">
<!--        <div class="top-nav">-->


<!--            --><?php //wp_nav_menu(array('container_class' => 'top-menu', 'theme_location' => 'top-menu', 'fallback_cb' => 'tie_nav_fallback')); ?>
<!--            --><?php //echo tie_alternate_menu(array('menu_name' => 'top-menu', 'id' => 'top-menu-mob')) ?>

            <div id="header-ust" style="min-width: 1035px; width: 1035px; float: left;">
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/sol.png'); width:24px; height:259px; float:left;"></div>
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/orta.png'); width:980px; height:259px; float:left;">
                    <div style="float: left; width: 200px; margin: 7px;">
                        <a title="<?php bloginfo('name'); ?>"
                        <a title="<?php bloginfo('name'); ?>" href="<?php echo home_url(); ?>/">
                            <img src="<?=get_template_directory_uri();?>/images/logo.png" alt="<?php bloginfo('name'); ?>" style="width: 250px;"/>
                        </a>
                    </div>
                    <div style="float: left;">
					<div id="slideshow" style="margin: 10px 0 0 45px;">
						<img src="<?=get_template_directory_uri();?>/caro/1.png" alt="" class="active" />
						<img src="<?=get_template_directory_uri();?>/caro/2.png" alt="" />
						<img src="<?=get_template_directory_uri();?>/caro/3.png" alt="" />
						<img src="<?=get_template_directory_uri();?>/caro/4.png" alt="" />
					</div>
					</div>
                </div>
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/sag.png'); width:29px; height:259px; float:left;"></div>
            </div>
            <div id="header-alt" style="min-width: 1035px; width: 1035px;">
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/menu-sol.png'); width:24px; height:56px; float:left;"></div>
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/menu-orta.png'); width:980px; height:56px; float:left;">
                    <?php $stick = ''; ?>
                    <?php if (tie_get_option('stick_nav')) $stick = 'class="fixed-enabled"' ?>
                    <nav id="main-nav"<?php echo $stick; ?>>
                        <?php wp_nav_menu(array('container_class' => 'main-menu', 'theme_location' => 'primary', 'fallback_cb' => 'tie_nav_fallback')); ?>
                        <?php echo tie_alternate_menu(array('menu_name' => 'primary', 'id' => 'main-menu-mob')) ?>
                        <?php if (tie_get_option('random_article')): ?>
                        <a href="<?php echo home_url(); ?>/?random" class="random-article ttip"
                           title="<?php _e('Random Article', 'tie') ?>"><?php _e('Random Article', 'tie') ?></a>
                        <?php endif ?>
                    </nav>
                    <!-- .main-nav /-->
                </div>
                <div style="background-image: url('<?=get_template_directory_uri();?>/images/menu-sag.png'); width:29px; height:56px; float:left;"></div>
            </div>



        </div>
    </header>
    <!-- #header /-->



    <?php tie_include('breaking-news'); // Get Breaking News template ?>

    <?php
    $sidebar = '';

    if (tie_get_option('sidebar_pos') == 'left') $sidebar = ' sidebar-left';
    if (is_single() || is_page()) {
        $get_meta = get_post_custom($post->ID);

        if (!empty($get_meta["tie_sidebar_pos"][0])) {
            $sidebar_pos = $get_meta["tie_sidebar_pos"][0];

            if ($sidebar_pos == 'left') $sidebar = ' sidebar-left';
            elseif ($sidebar_pos == 'full') $sidebar = ' full-width'; elseif ($sidebar_pos == 'right') $sidebar = ' sidebar-right';
        }
    }
    ?>
    <div id="main-content" class="container<?php echo $sidebar; ?>">