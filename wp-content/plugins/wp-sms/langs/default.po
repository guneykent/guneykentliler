msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-10-18 16:15+0330\n"
"PO-Revision-Date: 2013-10-18 16:15+0330\n"
"Last-Translator: Mostafa Soufi <mst404@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: F:\\Program Files\\xampp\\htdocs\\cms\\wordpress\\wp-"
"content\\plugins\\wp-sms\n"
"X-Generator: Poedit 1.5.5\n"
"X-Poedit-SearchPath-0: F:\\Program Files\\xampp\\htdocs\\cms\\wordpress\\wp-"
"content\\plugins\\wp-sms\n"

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/widget.php:2
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:31
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:46
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:109
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:146
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:218
msgid "Name"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:24
msgid "WP SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:25
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:44
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:95
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:160
msgid "Send SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:26
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:38
msgid "Members Newsletter"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:26
msgid "Newsletter subscribers"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:27
msgid "Setting"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:28
msgid "About"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:45
msgid "Rial"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:47
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:95
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:132
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:136
msgid "SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:87
#, php-format
msgid "Your Credit: %s %s"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:106
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:199
msgid "Credit"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:113
msgid "Newsletter Subscriber"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:120
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:171
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:179
#, php-format
msgid "Please check the <a href=\"%s\">SMS credit</a> the settings"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:132
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:133
msgid "Subscribe to SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:155
msgid "Subscribe SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:210
msgid "Suggested by SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:215
msgid "Your name"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:216
msgid "Your friend name"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:217
msgid "Your friend mobile"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:225
msgid "Send"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:235
#, php-format
msgid "Hi %s, the %s post suggested to you by %s. url: %s"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:237
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:83
msgid "SMS was sent with success"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:240
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:397
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:457
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:61
msgid "Please enter a valid mobile number"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:243
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:400
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:460
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/activation.php:24
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:64
msgid "Please complete all fields"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:266
#, php-format
msgid "WordPress %s is available! Please update now"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:291
msgid "A message was received from the Contact Form 7: "
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:291
msgid "(SMS wordpress plugin)"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:317
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:330
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:480
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:491
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:347
msgid "With success was removed"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:349
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:358
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:367
msgid "Not Found"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:356
msgid "User actived."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:365
msgid "User deactived."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:391
#, php-format
msgid "User <strong>%s</strong> was added successfully."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:394
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:58
msgid "Phone number is repeated"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:415
#, php-format
msgid "Group <strong>%s</strong> was added successfully."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:418
msgid "Group name is repeated"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:421
msgid "Please complete field"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:436
#, php-format
msgid "Group <strong>%s</strong> was successfully removed."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:440
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:54
msgid "Nothing found!"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/wp-sms.php:453
#, php-format
msgid "User <strong>%s</strong> was update successfully."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/activation.php:16
msgid "Your membership in the complete newsletter"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/activation.php:21
msgid "Security Code is wrong"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:27
msgid "Enter your information for SMS Subscribe"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:36
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:47
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:110
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:151
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:223
msgid "Mobile"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:41
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:23
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:48
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:111
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:156
msgid "Group"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:54
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:63
msgid "Subscribe"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:57
msgid "Unsubscribe"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/form.php:72
msgid "Subscribe is Deactive!"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:28
msgid "Your activation code"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:32
msgid "You will join the newsletter, Activation code sent to your number."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:34
msgid "Please enter the activation code:"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:42
msgid "You will join the newsletter"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/newsletter/newsletter.php:51
msgid "Your subscription was canceled."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:2
msgid "About Plugin"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:4
#, php-format
msgid "Version plugin: %s"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:5
msgid ""
"The first free WordPress Iranian plugin that works on Web service messages."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:6
#, php-format
msgid "This plugin created by %s from %s and %s gorup"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:7
msgid "do you have problem with plugin?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:8
#, php-format
msgid "if you have you can tell your problem in <a href=\"%s\">forum</a>."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:10
msgid "are you a translator?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:12
#, php-format
msgid "please send your translation to %s."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:14
msgid "you want to know about how to add your web service to this plugin?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/about.php:16
#, php-format
msgid "to adding your web service please contact with %s."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/meta-box.php:2
msgid "Send this post to subscribers?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/meta-box.php:4
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:147
msgid "Yes"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/meta-box.php:5
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:150
msgid "No"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:87
msgid "Please enter a message"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:98
msgid "Send from"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:102
msgid "Send to"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:107
msgid "Subscribe users"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:109
msgid "Number(s)"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:116
#, php-format
msgid "All (%s subscribers active)"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:126
msgid "For example"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:135
msgid "The remaining words"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:138
msgid "Your credit"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:144
msgid "Send a Flash"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/send-sms.php:153
msgid "Flash is possible to send messages without being asked, opens"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:17
msgid "SMS Setting"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:21
msgid "General Setting"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:23
msgid "Your Mobile Number"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:30
msgid "Your mobile country code"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:33
msgid "Enter your mobile country code. (For example: Iran 09, Australia 61)"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:37
msgid "Credit SMS Setting"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:39
msgid "Web Service"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:42
msgid "Select your Web Service"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:45
msgid "Iran"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:50
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:54
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:58
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:62
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:66
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:70
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:74
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:78
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:82
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:86
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:90
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:94
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:98
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:102
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:106
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:110
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:114
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:118
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:122
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:126
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:130
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:134
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:138
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:142
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:151
#, php-format
msgid "Web Service (%s)"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:146
msgid "Australia"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:155
msgid "For more information about adding Web Service"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:161
msgid "Select"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:167
msgid "Username"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:170
msgid "Your username in"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:173
#, php-format
msgid ""
"If you do not have a username for this service <a href=\"%s\">click here..</"
"a>"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:179
msgid "Password"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:182
msgid "Your password in"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:185
#, php-format
msgid ""
"If you do not have a password for this service <a href=\"%s\">click here..</"
"a>"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:191
msgid "Number"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:194
msgid "Your SMS sender number in"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:206
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:49
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:112
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:239
msgid "Status"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:209
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:222
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:230
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:238
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:246
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:256
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:265
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:274
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:123
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:242
msgid "Active"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:211
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:124
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:243
msgid "Deactive"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:217
msgid "Newsletter"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:219
msgid "Register?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:227
msgid "Send activation code via SMS?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:235
msgid "Posts sent to subscribers?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:243
msgid "Calling jQuery in Wordpress?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:247
msgid "Enable this option with JQuery is called in the theme"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:251
msgid "Post Suggestion"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:253
msgid "Suggested post by SMS?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:260
msgid "Notification"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:262
msgid "Notification SMS of a new wordPress version?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:266
msgid ""
"Enable this option with When a new version of WordPress was ready, will be "
"informed via SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:271
msgid "Notification SMS when messages received from Contact Form 7 plugin?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:275
msgid ""
"Enable this option with When a new message received of Contact Form 7 "
"plugin, will be informed via SMS"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/setting.php:284
msgid "Update"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:8
msgid "Are you sure?"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:23
msgid "Outset Create group to better manage the subscribers."
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:38
msgid "Subscriber"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:45
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:108
msgid "Register date"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:50
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:94
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:113
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:249
msgid "Edit"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:100
msgid "Not Found!"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:121
msgid "Bulk Actions"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:122
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:203
msgid "Remove"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:126
msgid "Apply"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:136
msgid "Page"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:136
msgid "From"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:144
msgid "Add new subscribe:"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:167
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:182
msgid "Add"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:175
msgid "Add new Group:"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:177
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:192
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:228
msgid "Group name"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:190
msgid "Delete Group:"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:216
msgid "Edit subscribe:"
msgstr ""

#: F:\Program
#: Files\xampp\htdocs\cms\wordpress\wp-content\plugins\wp-sms/setting/subscribes.php:254
msgid "Back"
msgstr ""
